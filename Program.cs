﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Hangfire;
using Hangfire.PostgreSql;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Npgsql;

namespace hangfire
{
    class Program
    {
        private static string CONNECTION_STRING = "";

        public static Task Main(string[] args) => CreateWebHostBuilder(args).Build().RunAsync();

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) => WebHost.CreateDefaultBuilder(args)
            .ConfigureServices(services =>
            {
                services.AddHangfire(config => config.UsePostgreSqlStorage(CONNECTION_STRING));
                services.AddMvc();
            }).Configure(app =>
            {
                app.UseHangfireDashboard();
                app.UseHangfireServer();
                app.UseMvc();

                BackgroundJob.Enqueue(() => CheckJob());
            }).ConfigureLogging((hostingContext, logging) =>
            {
                logging.AddFilter("Microsoft", LogLevel.Critical);
                logging.AddConsole();
            });

        [DisplayName("Проверка тасок в базе")]
        public static async Task CheckJob()
        {
            using (var conn = new NpgsqlConnection(CONNECTION_STRING))
            {
                var job = await conn.QueryFirstAsync<MyJob>("SELECT * from jobs LIMIT 1");
                BackgroundJob.Enqueue(() => DoJob(JobCancellationToken.Null, job));

                if (job != null)
                {
                    BackgroundJob.Enqueue(() => DoJob(JobCancellationToken.Null, job));
                }
            }
        }
        
        [DisplayName("Выполнение задачи")]
        public static async Task DoJob(IJobCancellationToken cancellationToken, MyJob myJob)
        {            
            await Task.Delay(5000);

            cancellationToken.ThrowIfCancellationRequested();

            Console.WriteLine($"Выполнение задачи {myJob.Name}");
            
            await Task.Delay(5000);
        }
    }

    public class MyJob
    {
        public string Name { get; set; }
    }
}